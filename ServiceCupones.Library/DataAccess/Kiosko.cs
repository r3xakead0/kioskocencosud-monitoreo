﻿using System.Collections.Generic;
using System.Data;
using System;
using BE = ServiceCupones.Library.BusinessEntity;

namespace ServiceCupones.Library.DataAccess
{
    public class Kiosko
    {
   
        /// <summary>
        /// Registrar el monitoreo
        /// </summary>
        /// <param name="beKiosko">Entidad Kiosko Monitoreo</param>
        /// <returns></returns>
        public int Registrar(BE.Kiosko beKiosko)
        {
            try
            {
                int rowsAffected = 0;

                if (beKiosko != null)
                {

                    string strFechaHora = beKiosko.Consulta.ToString("yyyy-MM-dd HH:mm:ss");

                    string query = "";
                    query += "INSERT INTO Auditoria.Kiosko_Monitoreo (Empresa,Tienda, Hostname, Ip, Conexion, Version, Sesion, Impreso, Restante, FechaHora) ";
                    query += $"VALUES ('{beKiosko.Empresa}','{beKiosko.Tienda}', '{beKiosko.Hostname}', '{beKiosko.Ip}', '{beKiosko.Conexion.Equals("SI")}', '{beKiosko.Version}', '{beKiosko.Sesion}', 0, 0, CAST('{strFechaHora}' AS DATETIME))";

                    rowsAffected = Util.MssqlHelper.ExecuteNonQuery(query);
                    
                }

                return rowsAffected;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Listar todos los kioscos activos
        /// </summary>
        /// <returns></returns>
        public List<BE.Kiosko> Listar()
        {
            try
            {
                var lstBeKioskos = new List<BE.Kiosko>();

                string query = "SELECT DISTINCT " +
                    "T0.Compania, T0.Tienda, T0.hostname " +
                    "FROM Perfil.Config T0 " +
                    "WHERE T0.Activo = 1 " +
                    "ORDER BY T0.Compania, T0.Tienda";

                var dtKioskos = Util.MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drKiosko in dtKioskos.Rows)
                {
                    var kiosko = new BE.Kiosko()
                    {
                        Empresa = drKiosko["Compania"].ToString(),
                        Tienda = drKiosko["Tienda"].ToString(),
                        Hostname = drKiosko["hostname"].ToString(),
                        Ip = "",
                        Conexion = "NO",
                        Version = "",
                        Sesion = ""
                    };

                    lstBeKioskos.Add(kiosko);
                }

                return lstBeKioskos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
