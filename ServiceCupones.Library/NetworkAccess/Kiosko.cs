﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace ServiceCupones.Library.NetworkAccess
{
    public class Kiosko
    {

        const int PORT_NO = 9876;

        private enum Command
        {
            ALL, //TEMPORAL
            STATUS //Devuelve Estado de Conexion, Version de Software y Sesion Actual
        }

        public string Conexion = "NO";
        public string Encendido = "NO";
        public string Version = "";
        public string Sesion = "";
        public string Ip = "";

        public Kiosko(string hostname)
        {
            string jsonAll = "";
            try
            {

                string ipAddress = "";
                jsonAll = this.SendCommand(hostname, Command.STATUS, out ipAddress);

                if (jsonAll.Trim().Length == 0) //TEMPORAL
                    jsonAll = this.SendCommand(hostname, Command.ALL, out ipAddress);

                if (jsonAll.Trim().Length > 0)
                {
                    var jsonDefinition = new { Version = "", Type = "", Number = "" };
                    var objDefinition = JsonConvert.DeserializeAnonymousType(jsonAll, jsonDefinition);

                    if (objDefinition != null)
                    {
                        this.Conexion = "SI";
                        this.Encendido = "SI";
                        this.Version = objDefinition.Version;
                        this.Sesion = objDefinition.Number;
                        this.Ip = ipAddress;
                    }
                    else
                    {

                        if (this.PingHost(hostname, out ipAddress))
                        {
                            this.Conexion = "SI";
                            this.Ip = ipAddress;
                        }
                        else
                        {
                            this.Conexion = "NO";
                            this.Ip = "";
                        }

                        this.Encendido = "NO";
                        this.Version = "";
                        this.Sesion = "";
                       
                    }
                }
                else
                {
                    if (this.PingHost(hostname, out ipAddress))
                    {
                        this.Conexion = "SI";
                        this.Ip = ipAddress;
                    }
                    else
                    {
                        this.Conexion = "NO";
                        this.Ip = "";
                    }

                    this.Encendido = "NO";
                    this.Version = "";
                    this.Sesion = "";
                }
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string SendCommand(string hostname, Command command, out string ip)
        {
            try
            {
                string response = "";
                string ipAddress = "";

                using (var client = new TcpClient())
                {

                    var result = client.BeginConnect(hostname, PORT_NO, null, null);
                    result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(3));

                    if (client.Connected)
                    {
                        byte[] bytesToSend, bytesToRead;
                        NetworkStream nwStream;
                        int bytesRead = 0;

                        string textToSend = command.ToString();
                        bytesToSend = ASCIIEncoding.ASCII.GetBytes(textToSend);

                        nwStream = client.GetStream();
                        nwStream.Write(bytesToSend, 0, bytesToSend.Length);

                        bytesToRead = new byte[client.ReceiveBufferSize];
                        bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
                        string textToRead = Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);

                        client.EndConnect(result);

                        response = textToRead.Trim();
                        ipAddress = ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString();
                    }
                   
                }

                ip = ipAddress;
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool PingHost(string hostname, out string ip)
        {
            bool pingable = false;
            Ping pinger = null;

            string ipAddress = "";
            try
            {
                pinger = new Ping();
                PingReply reply = pinger.Send(hostname);

                if (reply.Status == IPStatus.Success)
                {
                    pingable = true;
                    ipAddress = reply.Address.ToString();
                }
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            finally
            {
                if (pinger != null)
                {
                    pinger.Dispose();
                }
            }

            ip = ipAddress;
            return pingable;
        }
    }
}
