﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BE = ServiceCupones.Library.BusinessEntity;
using DA = ServiceCupones.Library.DataAccess;
using DN = ServiceCupones.Library.NetworkAccess;

namespace ServiceCupones.Library.BusinessLogic
{
    public class Kiosko
    {
        public Kiosko()
        {

        }


        /// <summary>
        /// Listar los kioscos activos
        /// </summary>
        /// <returns></returns>
        public List<BE.Kiosko> Monitoreo()
        {
            try
            {
                var lstBeKioskos = new DA.Kiosko().Listar();
                int cntKioskos = lstBeKioskos.Count;

                var tasks = new List<Task<BE.Kiosko>>();
                for (int i = 0; i < cntKioskos; i++)
                {
                    var beKiosko = lstBeKioskos[i];
                    tasks.Add(
                        Task.Factory.StartNew(() => this.Estado(beKiosko), TaskCreationOptions.LongRunning)
                    );
                }

                try
                {
                    Task.WaitAll(tasks.ToArray(), 60000);

                    for (int i = 0; i < cntKioskos; i++)
                    {
                        if(tasks[i].IsCompleted)
                            lstBeKioskos[i] = tasks[i].Result;
                    }
                }
                catch (AggregateException ae)
                {
                    throw ae.Flatten();
                }

                Task.Delay(60000).Wait();
                return lstBeKioskos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private BE.Kiosko Estado(BE.Kiosko beKiosko)
        {
            try
            {
                BE.Kiosko kiosko = null;

                if (beKiosko != null)
                {
                    DN.Kiosko dnKiosko = new DN.Kiosko(beKiosko.Hostname);

                    kiosko = beKiosko;
                    kiosko.Conexion = dnKiosko.Conexion;
                    kiosko.Encendido = dnKiosko.Encendido;
                    kiosko.Version = dnKiosko.Version;
                    kiosko.Sesion = dnKiosko.Sesion;
                    kiosko.Ip = dnKiosko.Ip;
                    kiosko.Consulta = DateTime.Now;

                }

                return kiosko;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Registrar(BE.Kiosko beKiosko)
        {
            try
            {
                int rowsAffected = new DA.Kiosko().Registrar(beKiosko);
                return rowsAffected > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

    }
}
