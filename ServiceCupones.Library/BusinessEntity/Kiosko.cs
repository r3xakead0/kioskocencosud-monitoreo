﻿using System;

namespace ServiceCupones.Library.BusinessEntity
{
    public class Kiosko
    {
        public string Empresa { get; set; } = "";
        public string Tienda { get; set; } = "";
        public string Hostname { get; set; } = "";
        public string Ip { get; set; } = "";
        public string Conexion { get; set; } = "NO";
        public string Encendido { get; set; } = "NO";
        public string Version { get; set; } = "";
        public string Sesion { get; set; } = "";
        public DateTime Consulta { get; set; } 
    }
}
