﻿namespace ServiceCupones.Console.Entities
{
    public class Kiosko
    {
        public string NRO { get; set; }
        public string EMPRESA { get; set; }
        public string TIENDA { get; set; }
        public string HOSTNAME { get; set; }
        public string IP { get; set; }
        public string CONEXION { get; set; }
        public string ENCENDIDO { get; set; }
        public string VERSION { get; set; }
        public string SESION { get; set; }
        public string CONSULTA { get; set; }
    }
}
