﻿using System;
using BL = ServiceCupones.Library.BusinessLogic;
using System.Collections.Generic;
using System.Linq;
using ConTabs;
using ServiceCupones.Console.Entities;

namespace ServiceCupones.Console
{
    class Program
    {

        const int PORT_NO = 9876;

        static void Main(string[] args)
        {
            try
            {

                bool registrar = true;
                if (args.Length == 1)
                {
                    bool rpta = false;
                    if (bool.TryParse(args[0], out rpta))
                        registrar = rpta;
                    else
                        registrar = false;
                }
                    
                while (true)
                {
                    try
                    {

                        var blKiosko = new BL.Kiosko();

                        //Definir variables
                        var lstKioskos = new List<Kiosko>();
                        var lstResumen = new List<Resumen>();
                        int cantidadConectados = 0;
                        int cantidadDesconectados = 0;
                        int cantidadEncendidos = 0;
                        int cantidadApagados = 0;

                        //Cargar variables
                        var lstBeKioskos = blKiosko.Monitoreo();

                        cantidadConectados = lstBeKioskos.Where(x => x.Conexion.Equals("SI")).Count();
                        cantidadDesconectados = lstBeKioskos.Where(x => x.Conexion.Equals("NO")).Count();
                        cantidadEncendidos = lstBeKioskos.Where(x => x.Encendido.Equals("SI")).Count();
                        cantidadApagados = lstBeKioskos.Where(x => x.Encendido.Equals("NO")).Count();

                        lstResumen.Add(new Resumen() { ESTADO = "SI", CONEXION = cantidadConectados.ToString(), ENCENDIDO = cantidadEncendidos.ToString() });
                        lstResumen.Add(new Resumen() { ESTADO = "NO", CONEXION = cantidadDesconectados.ToString(), ENCENDIDO = cantidadApagados.ToString() });

                        int nro = 1;
                        foreach (var beKiosko in lstBeKioskos)
                        {

                            if (registrar)
                            {
                                var rpta = blKiosko.Registrar(beKiosko);
                            }

                            var kiosko = new Kiosko();

                            kiosko.NRO = nro.ToString();
                            kiosko.EMPRESA = beKiosko.Empresa;
                            kiosko.TIENDA = beKiosko.Tienda;
                            kiosko.HOSTNAME = beKiosko.Hostname;
                            kiosko.IP = beKiosko.Ip;
                            kiosko.CONEXION = beKiosko.Conexion;
                            kiosko.ENCENDIDO = beKiosko.Encendido;
                            kiosko.VERSION = beKiosko.Version;
                            kiosko.SESION = beKiosko.Sesion;
                            kiosko.CONSULTA = beKiosko.Consulta.ToString("dd/MM/yyyy HH:mm:ss");

                            lstKioskos.Add(kiosko);
                            nro++;
                        }

                        var resumen = Table<Resumen>.Create(lstResumen);
                        resumen.Columns["ESTADO"].Alignment = Alignment.Center;
                        resumen.Columns["CONEXION"].Alignment = Alignment.Right;
                        resumen.Columns["ENCENDIDO"].Alignment = Alignment.Right;
                        resumen.TableStyle = Style.UnicodeLines;

                        var kioskos = Table<Kiosko>.Create(lstKioskos);
                        kioskos.Columns["NRO"].Alignment = Alignment.Right;
                        kioskos.Columns["EMPRESA"].Alignment = Alignment.Left;
                        kioskos.Columns["TIENDA"].Alignment = Alignment.Left;
                        kioskos.Columns["HOSTNAME"].Alignment = Alignment.Left;
                        kioskos.Columns["IP"].Alignment = Alignment.Center;
                        kioskos.Columns["CONEXION"].Alignment = Alignment.Center;
                        kioskos.Columns["ENCENDIDO"].Alignment = Alignment.Center;
                        kioskos.Columns["VERSION"].Alignment = Alignment.Center;
                        kioskos.Columns["SESION"].Hide = true;
                        kioskos.Columns["CONSULTA"].Hide = true;
                        kioskos.TableStyle = Style.UnicodeLines;

                        //Mostrar la información del monitoreo
                        System.Console.Clear();

                        System.Console.BackgroundColor = ConsoleColor.DarkCyan;
                        System.Console.ForegroundColor = ConsoleColor.White;
                        System.Console.WriteLine(CentrarTexto("MONITOREO DE KIOSKOS", System.Console.WindowWidth));
                        System.Console.ResetColor();

                        System.Console.WriteLine();
                        System.Console.WriteLine(resumen.ToString());
                        System.Console.WriteLine();
                        System.Console.WriteLine(kioskos.ToString());
                        System.Console.WriteLine();

                        System.Console.BackgroundColor = ConsoleColor.DarkCyan;
                        System.Console.ForegroundColor = ConsoleColor.White;
                        System.Console.WriteLine(CentrarTexto($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}", System.Console.WindowWidth));
                        System.Console.ResetColor();

                    }
                    catch (Exception exInner)
                    {
                        System.Console.Clear();

                        System.Console.BackgroundColor = ConsoleColor.DarkGreen;
                        System.Console.ForegroundColor = ConsoleColor.White;
                        System.Console.WriteLine(CentrarTexto("MONITOREO DE KIOSKOS", System.Console.WindowWidth));
                        System.Console.ResetColor();

                        System.Console.WriteLine();

                        System.Console.BackgroundColor = ConsoleColor.DarkRed;
                        System.Console.ForegroundColor = ConsoleColor.White;
                        System.Console.WriteLine($"{ "ERROR".PadRight(10) }:");
                        System.Console.WriteLine($"{ exInner.Message }");
                        System.Console.ResetColor();
                    }

                }
                
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"No se puedo ejecutar el aplicativo por el error :{ex.Message}");
                System.Console.ReadLine();
            }
        }

        static string CentrarTexto(string texto, int longitud)
        {
            int spaces = longitud - texto.Length;
            int padLeft = spaces / 2 + texto.Length;

            return texto.PadLeft(padLeft).PadRight(longitud);
        }
    }
}
